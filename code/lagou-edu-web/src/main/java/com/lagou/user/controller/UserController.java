package com.lagou.user.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.lagou.entity.*;
import com.lagou.user.UserService;
import org.csource.fastdfs.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.lagou.entity.FileSystem;
import org.csource.common.NameValuePair;
import java.io.IOException;


/**
 * @BelongsProject: lagou-edu-web
 * @Author: GuoAn.Sun
 * @CreateTime: 2020-09-08 11:22
 * @Description:
 */

@RestController
@RequestMapping("user")
public class UserController {

    @Reference // 远程消费
    private UserService userService;

    @GetMapping("login")
    public UserDTO login(String phone, String password,String nickname,String headimg) {
        UserDTO dto = new UserDTO();
        User user = null;
        System.out.println("phone = " + phone);
        System.out.println("password = " + password);
        System.out.println("nickname = " + nickname);

        // 检测手机号是否注册
        Integer i = userService.checkPhone(phone);
        if(i == 0){
            // 未注册，自动注册并登录
            userService.register(phone, password,nickname,headimg);
            dto.setMessage("手机号尚未注册，系统已帮您自动注册，请牢记密码！");
            user = userService.login(phone, password);
        }else{
            user = userService.login(phone, password);
            if(user == null){
                dto.setState(300); //300表示失败
                dto.setMessage("帐号密码不匹配，登录失败！");
            }else{
                dto.setState(200); //200表示成功
                dto.setMessage("登录成功！");
            }
        }
        dto.setContent(user);
        return dto;
    }

    @GetMapping( "updatePassword")
    public UserDTO2 updatePassword(int userid,String password){
        UserDTO2 dto=new UserDTO2();
        Integer i=userService.updatePassword(userid,password);
        if(i==0){
            dto.setMessage("修改密码失败，请检查后重试");
            dto.setState(300);
            dto.setSuccess(false);
        }else{
            dto.setMessage("修改成功");
            dto.setState(200);
            dto.setSuccess(true);
        }
        return dto;
    }

    @GetMapping(value = "updateUserInfo")
    public UserDTO2 updateUserInfo(int userid,String portrait,String name){
        UserDTO2 dto=new UserDTO2();
        System.out.println(userid);
        System.out.println(portrait);
        System.out.println(name);
        Integer i=userService.updateUserInfo(userid,portrait,name);
        if(i==0){
            dto.setMessage("修改个人信息失败");
            dto.setState(300);
            dto.setSuccess(false);
        }else{
            dto.setMessage("修改成功");
            dto.setState(200);
            dto.setSuccess(true);
        }
        return dto;
    }


    @PostMapping("upload")
    @ResponseBody
    public FileSystem upload(@RequestParam("file") MultipartFile file) throws IOException {
        System.out.println("接收到：" +file);
        FileSystem fs = new FileSystem();

        //获得文件的原始名称
        String oldFileName = file.getOriginalFilename();
        //获得后缀名
        String hou = oldFileName.substring(oldFileName.lastIndexOf(".")+1);


        try {
            //加载配置文件
            ClientGlobal.initByProperties("config/fastdfs-client.properties");
            //创建tracker客户端
            TrackerClient tc = new TrackerClient();
            //根据tracker客户端创建连接
            TrackerServer ts = tc.getConnection();
            StorageServer ss = null;
            //定义storage客户端
            StorageClient1 client = new StorageClient1(ts, ss);
            //文件元信息
            NameValuePair[] list = new NameValuePair[1];
            list[0] = new NameValuePair("fileName", oldFileName);
            //上传，返回fileId
            String fileId = client.upload_file1(file.getBytes(), hou, list);
            System.out.println(fileId);
            ts.close();
            //封装数据对象，将路径保存到数据库（本次不写）
            fs.setFileId(fileId);
            fs.setFilePath(fileId);
            fs.setFileName(oldFileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fs;
    }
}
