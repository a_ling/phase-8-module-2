package user;

import com.lagou.entity.User;
import mapper.UserDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.swing.*;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @BelongsProject: lagou-edu
 * @Author: GuoAn.Sun
 * @CreateTime: 2020-09-07 17:59
 * @Description:
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/spring-dao.xml" })
public class TestUser {

    @Autowired
    private UserDao userDao;
    
    @Test
    public void login(){
//        User user = userDao.login("110", "123");
//        System.out.println("user = " + user);
    }


    @Test
    public void checkPhone(){
//        Integer i = userDao.checkPhone("1101");
//        System.out.println("i = " + i); //0：未注册 ， 1：已注册
    }

    @Test
    public void register(){
//        Integer i = userDao.register("114", "123");
//        System.out.println("i = " + i); //0：注册失败 ， 1：注册成功
    }

    @Test
    public void updatePassword(){
//        Date dt = new Date();
//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String currentTime = sdf.format(dt);
//        Integer i=userDao.updatePassword(100030011,"1234",currentTime);
//        System.out.println("i="+i);
    }

    @Test
    public void updateUserInfo(){
//        Date dt = new Date();
//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String currentTime = sdf.format(dt);
//        Integer i = userDao.updateUserInfo(100030018, "头像新地址", "吕布",currentTime);
//        System.out.println("i="+i);
    }

}
