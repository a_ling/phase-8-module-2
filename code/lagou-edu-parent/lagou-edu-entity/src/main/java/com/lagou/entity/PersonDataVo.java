package com.lagou.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PersonDataVo {
    private Integer userid;
    //头像新地址
    private String portrait;
    //新昵称
    private String name;
}
