package com.lagou.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Author : liuchangling
 * @Descrition :
 * @Date： Created in 3:50 下午 2021/8/23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserDTO2 implements Serializable {
    private int state;  // 操作状态
    private String message;  // 状态描述
    private boolean success;  // 是否成功
}